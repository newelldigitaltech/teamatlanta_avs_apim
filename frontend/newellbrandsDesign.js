$(document).ready(function(){


$( "#hint" ).click(function() {
   document.getElementById("address-line1").value = "3 Glenlake Pkwy";
   document.getElementById("country").value = "US";
   document.getElementById("address-line2").value = "";
   document.getElementById("city").value = "Atlanta";
   document.getElementById("zip-code").value = "30328";
   document.getElementById("state").value = "GA";

});

$( "#submit-request" ).click(function() {


  var country = document.getElementById("country").value;
  var address1 = document.getElementById("address-line1").value;
  var address2 = document.getElementById("address-line2").value;
  var city = document.getElementById("city").value;
  var state = document.getElementById("state").value;
  var zipCode = document.getElementById("zip-code").value;

  var request =  {
				  "header": {
								"apiKey": "a30e71c0-240f-4e0a-8065-26e9573d5013",
								"dateTime": "",
								"requestID": "1234567890abcdefg",
								"country": "USA"
							},
				   "address": {
								  "street1": address1,
								  "street2": address2,
								  "city": city,
								  "country": "US",
								  "state": state,
								  "postalCode": zipCode
							 }
				  }

  var obj = JSON.stringify(request);

  var lookupType = document.getElementById("lookup").value;



var url = "";

if(lookupType == "OMS"){
	url = "http://bibinvijayaratnan-trial-prod.apigee.net/avs/oms/validateaddress";
}else if (lookupType == "Web"){
	url = "http://bibinvijayaratnan-trial-prod.apigee.net/avs/web/validateaddress";
}else{
	url = "http://bibinvijayaratnan-trial-prod.apigee.net/avs/cs/validateaddress";
}


		var element = document.getElementById("output-placeholder");

		$.ajax({
			url: url,
			dataType: 'json',
			data: obj,
			type: 'POST',
			crossDomain : true,
			success: function(data) {

				var output = JSON.stringify(data);
				if(output.length >= 10){

					 document.getElementById("output-placeholder").style.overflow="scroll";
					 document.getElementById("output-placeholder").style.height="400px";


				}else{
					 document.getElementById("output-placeholder").style.height="";
					 document.getElementById("output-placeholder").style.overflow="";
				}

			  $("#output-placeholder").html(JSON.stringify(data, null, 4));

			}
		});
});

});
